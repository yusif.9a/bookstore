FROM openjdk
RUN echo "hello world"
COPY ./build/libs/bookstore-0.1.jar /app/
WORKDIR /app/


ENTRYPOINT ["java"]
CMD ["-jar", "/app/bookstore-0.1.jar"]