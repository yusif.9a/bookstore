package com.bookstore.jwt;

import com.bookstore.model.User;
import io.jsonwebtoken.Claims;

public interface JwtService {

    String issueToken(User user);

    Claims verifyToken(String token);
}
