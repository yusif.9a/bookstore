package com.bookstore.config;

import com.bookstore.filter.FilterConfigureAdaptor;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.http.HttpMethod.*;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class WebSecurityConfig {

    public static final String ADMIN = "ADMIN";
    private final FilterConfigureAdaptor configureAdaptor;

    @Bean
    public static PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationManager authenticationManager(
            AuthenticationConfiguration configuration) throws Exception {
        return configuration.getAuthenticationManager();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

        http.apply(configureAdaptor);

        String role_admin = "ROLE_ADMIN";
        String role_student = "ROLE_STUDENT";
        String role_author = "ROLE_AUTHOR";

        http
                .csrf()
                .disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

                .and()

                .authorizeRequests()
                .antMatchers("/user/sign-up-student")
                .permitAll()

                .and()

                .authorizeRequests()
                .antMatchers("/user/login")
                .permitAll()

                .and()

                .authorizeRequests()
                .antMatchers("/user/sign-up-author")
                .permitAll()


                .and()

                .authorizeRequests()
                .antMatchers("/book/create-book")
                .hasAnyAuthority(role_admin, role_author)
                .and()

                .authorizeRequests()
                .antMatchers(DELETE, "/book/delete/**")
                .hasAnyAuthority(role_admin, role_author)
                .and()

                .authorizeRequests()
                .antMatchers(GET, "/student/find-studet/**")
                .hasAnyAuthority(role_author)
                .and()

                .authorizeRequests()
                .antMatchers("/student/add-book/")
                .hasAnyAuthority(role_student)

                .and()
                .httpBasic();
        return http.build();
    }


//    @Bean
//    public UserDetailsService users() {
//        // The builder will ensure the passwords are encoded before saving in memory
//        User.UserBuilder users = User.withDefaultPasswordEncoder();
//        UserDetails user = users
//                .username("user")
//                .password("password")
//                .roles("USER")
//                .build();
//        UserDetails admin = users
//                .username("admin")
//                .password("password")
//                .roles("USER", "ADMIN")
//                .build();
//        return new InMemoryUserDetailsManager(user, admin);
//    }
}
