package com.bookstore.dto;

import com.bookstore.model.Student;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

public class StudentDto {



    String name;
    String age;
    String email;

    ListBook readBooks;

}
