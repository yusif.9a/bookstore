package com.bookstore.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;
import static lombok.AccessLevel.PRIVATE;

@Entity
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
@FieldDefaults(level = PRIVATE)
@NamedEntityGraph(name = "User.authorities", attributeNodes = @NamedAttributeNode("authorities"))
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @NotNull(message = "username cannot be empty")
    String username;
    @NotNull(message = "Password cannot be empty")
//    @Length(min = 7, message = "Password should be at last 7 character")
    @NotNull(message = "Please Enter Hole area")
    String name;
    String password;
    @Email
    @NotNull(message = "Please enter a valid email")
    @Column(name = "email", unique = true)
    String email;
    String address;
    String gender;
    @NotNull
    Integer age;
    boolean isAccountNonExpired;
    boolean isAccountNonLocked;
    boolean isCredentialsNonExpired;
    boolean isEnabled;
    @OneToMany(
            fetch = LAZY,
            cascade = ALL,
            mappedBy = "user"
    )
    @ToString.Exclude
    @JsonBackReference
    private Set<Role> authorities;

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        User user = (User) object;
        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}