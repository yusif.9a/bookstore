package com.bookstore.repository;

import com.bookstore.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Long> {

    Student findByEmail(String email);
    Student findByName(String email);

}
