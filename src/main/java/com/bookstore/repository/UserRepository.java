package com.bookstore.repository;

import com.bookstore.model.Role;
import com.bookstore.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

    boolean existsByUsername(String username);
    boolean existsByEmail(String email);

//    User findByAuthorities(Set<Role> authorities);
}
