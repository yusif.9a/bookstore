package com.bookstore.controller;


import com.bookstore.dto.LoginDto;
import com.bookstore.model.User;
import com.bookstore.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {
    private final UserService userService;


    @PostMapping("/sign-up-student")
    public ResponseEntity<User> signUpAsStudent(@RequestBody User user) {

        return new ResponseEntity<>(userService.signUpAsStudent(user), HttpStatus.CREATED);
    }

    @PostMapping("/sign-up-author")
    public ResponseEntity<User> signUpAsAuthor(@RequestBody User user) {

        return new ResponseEntity<>(userService.signUpAsAuthor(user), HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginDto loginDto) {
        return new ResponseEntity<>(userService.login(loginDto), HttpStatus.OK);
    }

}
