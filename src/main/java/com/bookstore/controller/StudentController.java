package com.bookstore.controller;


import com.bookstore.dto.StudentDto;
import com.bookstore.model.Student;
import com.bookstore.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequiredArgsConstructor
@RequestMapping("/student")
public class StudentController {

    private final StudentService studentService;

    @PostMapping("/add-book/{bookName}")

    public ResponseEntity<Student> addBook(@PathVariable String bookName, Principal principal){

        return new ResponseEntity<>(studentService.addNewBook(bookName, principal), HttpStatus.OK);
    }


    @GetMapping("/find-studet/{bookName}")
    public ResponseEntity<Student> addBook(@PathVariable String bookName){

        return new ResponseEntity<>(studentService.findStudentAllBooks(bookName), HttpStatus.OK);
    }


}
