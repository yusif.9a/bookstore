package com.bookstore.mapped;


import com.bookstore.dto.StudentDto;
import com.bookstore.model.Student;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface StudentMapper {
    Student studentDtoToStudent(StudentDto studentDto);
    StudentDto studentToStudentDto(Student student);
}
