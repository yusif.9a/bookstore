package com.bookstore.service;

import com.bookstore.dto.LoginDto;
import com.bookstore.model.User;

public interface UserService {

    User signUpAsStudent(User user);
    User signUpAsAuthor(User user);
    String login(LoginDto loginDto);

}
