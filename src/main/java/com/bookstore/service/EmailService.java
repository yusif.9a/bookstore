package com.bookstore.service;

public interface EmailService {
    public void sendEMail(String toEmail, String subject, String message);
}
