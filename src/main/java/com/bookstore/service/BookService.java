package com.bookstore.service;

import com.bookstore.model.Book;

public interface BookService {

    Book createBook(Book book);

    String deleteBook(Long id);
}
