package com.bookstore.service;

import com.bookstore.dto.StudentDto;
import com.bookstore.model.Book;
import com.bookstore.model.Student;

import java.security.Principal;

public interface StudentService {

    Student addNewBook(String bookName, Principal principal);

    Student findStudentAllBooks(String name);



}
