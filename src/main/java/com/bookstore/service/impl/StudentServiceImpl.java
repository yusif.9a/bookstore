package com.bookstore.service.impl;

import com.bookstore.dto.ListBook;
import com.bookstore.dto.StudentDto;
import com.bookstore.mapped.StudentMapper;
import com.bookstore.model.Book;
import com.bookstore.model.Student;
import com.bookstore.repository.BookRepository;
import com.bookstore.repository.StudentRepository;
import com.bookstore.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.util.List;
import java.util.Set;

@Slf4j

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final BookRepository bookRepository;

    private final StudentMapper studentMapper;


    @Override
    public Student addNewBook(String bookName, Principal principal) {

        if (!bookRepository.existsByName(bookName))
            throw new RuntimeException("That Book have not yet !");

        log.info(principal.getName()+"s");
        Student student = studentRepository.findByEmail(principal.getName().toString());
        Book book = bookRepository.findByName(bookName).get();

        student.getReadBook().add(book);

        studentRepository.save(student);
        return student;
    }

    @Override
    public Student findStudentAllBooks(String name) {

        Student student = studentRepository.findByName(name);

        return student;

    }
}
