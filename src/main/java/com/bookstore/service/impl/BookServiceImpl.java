package com.bookstore.service.impl;

import com.bookstore.model.Book;
import com.bookstore.model.Role;
import com.bookstore.model.Student;
import com.bookstore.model.User;
import com.bookstore.repository.BookRepository;
import com.bookstore.repository.RoleRepository;
import com.bookstore.repository.StudentRepository;
import com.bookstore.repository.UserRepository;
import com.bookstore.service.BookService;
import com.bookstore.service.EmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Set;


@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

//    private final UserRepository userRepository;
//    private final RoleRepository roleRepository;

    private final StudentRepository studentRepository;
    private final EmailService emailService;
    @Override
    public Book createBook(Book book) {
        if (book.getName().equals("") || book.getAuthor().equals("")){
            throw new RuntimeException("The field can not be empty");
        }



        if (bookRepository.existsByName(book.getName()))
            throw new RuntimeException("the name already exists");

        List<Student> student = studentRepository.findAll();

        student.stream().forEach(student1 -> {
          String mail = student1.getEmail().toString();

            String subject = "Book Store & New Books";
            String message = "Hi "+ student1.getName()+ " we have a new book our " + " Book name is "+book.getName() + " and writer "+book.getAuthor();
            emailService.sendEMail(mail, subject,message);
        });





//        String email = user.getEmail();


        return bookRepository.save(book);

    }

    @Override
    public String deleteBook(Long id) {
        if (bookRepository.existsById(id)){
            bookRepository.deleteById(id);
            return "The book is deleted";
        }

        return "The book not exists";
    }
}
