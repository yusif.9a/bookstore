package com.bookstore.service.impl;

import com.bookstore.dto.LoginDto;
import com.bookstore.jwt.JwtService;
import com.bookstore.model.Role;
import com.bookstore.model.SessionToken;
import com.bookstore.model.Student;
import com.bookstore.model.User;
import com.bookstore.repository.SessionTokenRepository;
import com.bookstore.repository.StudentRepository;
import com.bookstore.repository.UserRepository;
import com.bookstore.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepository userRepository;
    private final StudentRepository studentRepository;
    private final SessionTokenRepository sessionTokenRepository;
    private final JwtService jwtService;
    private final PasswordEncoder passwordEncoder;

    @Override
    public User signUpAsStudent(User user) {
        if (userRepository.existsByEmail(user.getEmail()) || userRepository.existsByUsername(user.getUsername())) {
            throw new RuntimeException("User already exists");
        }

        user.setUsername(user.getUsername().trim().toLowerCase());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setEmail(user.getEmail().trim().toLowerCase());
        user.setAddress(user.getAddress().toLowerCase());
        user.setGender(user.getGender().trim().toLowerCase());
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        ;
        user.setCredentialsNonExpired(true);
        user.setEnabled(true);

        user.getAuthorities().forEach(role -> {
            role.setAuthority("ROLE_STUDENT");
            role.setUser(user);
        });

        Student student = new Student();
        student.setName(user.getName().trim().toLowerCase());
        student.setAge(user.getAge());
        student.setEmail(user.getEmail().trim().toLowerCase());
        studentRepository.save(student);
        return userRepository.save(user);

    }

    @Override
    public User signUpAsAuthor(User user) {
        if (userRepository.existsByEmail(user.getEmail()) || userRepository.existsByUsername(user.getUsername())) {
            throw new RuntimeException("User already exists");
        }

        user.setUsername(user.getUsername().trim().toLowerCase());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setEmail(user.getEmail().trim().toLowerCase());
        user.setAddress(user.getAddress().toLowerCase());
        user.setGender(user.getGender().trim().toLowerCase());
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        ;
        user.setCredentialsNonExpired(true);
        user.setEnabled(true);

        Role role = new Role();
        user.getAuthorities().forEach(role1 -> {
            role1.setAuthority("ROLE_AUTHOR");
            role1.setUser(user);
        });

        return userRepository.save(user);
    }

    @Override
    public String login(LoginDto loginDto) {

        if (!userRepository.existsByUsername(loginDto.getUsername()))
            throw new RuntimeException("The user name or password not correctly");


        String password = loginDto.getPassword();
        User user = userRepository.findByUsername(loginDto.getUsername()).get();
        log.info("ddeee1");

        if (passwordEncoder.matches(password, user.getPassword())) {
            log.info("dde1ee");

            String token = jwtService.issueToken(user);
            SessionToken sessionToken = new SessionToken();
            log.info("dd2eee");

            sessionToken.setUserId(user.getId());
            sessionToken.setAccessToken(token);
            log.info(password);
            sessionTokenRepository.save(sessionToken);
            return "You already entire your account";
        }
        return "Your username or password not correctly";
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("You Have problem"));

    }
}
